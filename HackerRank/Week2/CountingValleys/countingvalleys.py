#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'countingValleys' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER steps
#  2. STRING path
#

def countingValleys(steps, path):
    # U = uphill step
    # D = downhill step
    # each step up/down = 1 unit in alt
    # start & end at sea level
    # Mountain = up then down
    # Valley = down then up
    # Initialize variable to track alt
    altitude = 0
    # Initialize variable to count valleys
    valley = 0

    # Iterate through each step in path
    for step in path:
        # if step = U
        if step == "U":
            # Increment alt count
            altitude += 1
        # if step = D
        elif step == "D":
            # Decrement alt
            altitude -= 1

        # Check if alt is 0 coming up from below sea
        if altitude == 0 and step == "U":
            # If yes, increment valley
            valley += 1

    # Return # of valleys walked through
    return valley

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    steps = int(input().strip())

    path = input()

    result = countingValleys(steps, path)

    fptr.write(str(result) + '\n')

    fptr.close()
