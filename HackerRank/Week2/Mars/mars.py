#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'marsExploration' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#

def marsExploration(s):
    # Determine how many letters changed
    # Initialize variable to track count
    changed = 0

    # Loop through string in steps of 3
    for x in range(0, len(s), 3):
        # Compare each character in the segment
        if s[x] != 'S':
            changed += 1
        if s[x + 1] != 'O':
            changed += 1
        if s[x + 2] != 'S':
            changed += 1
            
    return changed


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = marsExploration(s)

    fptr.write(str(result) + '\n')

    fptr.close()
