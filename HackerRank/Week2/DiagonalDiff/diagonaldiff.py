#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'diagonalDifference' function below.
#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY arr as parameter.
#

def diagonalDifference(arr):
    # Create variables
    length = len(arr)
    left_sum = 0
    right_sum = 0

    # Iterate through the rows & columns
    for x in range(length):
        # Add the element to the sum
        left_sum += arr[x][x]
        right_sum += arr[x][length-1-x]

    # Calculate absolute difference between the sums
    abs_diff = abs(left_sum - right_sum)
    return abs_diff

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = []

    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    result = diagonalDifference(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
