#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'gradingStudents' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY grades as parameter.
#

def gradingStudents(grades):
    # Each student receives a grade from 0 to 100
    # < 38 is fail
    # Round up if difference between grade & next multiple
    # of 5 is < 3 (84 round to 85, 85-84 < 3)
    #Create variable to store results
    final_grade = []
    # Iterate through each grade
    for grade in grades:
        # Fail condition
        if grade < 38:
            # Add to array
            final_grade.append(grade)
        # Other conditions
        else:
            next_multiple = ((grade + 4) // 5) * 5
            if next_multiple - grade < 3:
                final_grade.append(next_multiple)
            # If grade not rounded, append to array
            else:
                final_grade.append(grade)
    return final_grade


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    grades_count = int(input().strip())

    grades = []

    for _ in range(grades_count):
        grades_item = int(input().strip())
        grades.append(grades_item)

    result = gradingStudents(grades)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
