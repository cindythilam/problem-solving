#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'pangrams' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def pangrams(s):
    # Provide alphabet
    abc = set('abcdefghijklmnopqrstuvwxyz')
    # Lowercase letters in given string
    lower_s = s.lower()

    # Iterate through each letter
    for letter in abc:
        if letter not in lower_s:
            return "not pangram"
    return "pangram"


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = pangrams(s)

    fptr.write(result + '\n')

    fptr.close()
