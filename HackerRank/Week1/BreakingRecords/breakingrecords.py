#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'breakingRecords' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY scores as parameter.
#

def breakingRecords(scores):
    # Determine the # of times she breaks her records for max & min points
    # Declare variables as 0 count
    lowest = 0
    highest = 0
    # Initial scores in first game
    low_record = high_record = scores[0]
    # Go through each score after first game
    for score in scores:
        if score < low_record:
            low_record = score
            lowest += 1
        if score > high_record:
            high_record = score
            highest += 1
    # Return records for max & min points
    return [highest, lowest]

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    scores = list(map(int, input().rstrip().split()))

    result = breakingRecords(scores)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
