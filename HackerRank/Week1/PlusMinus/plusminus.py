import math
import os
import random
import re
import sys

#
# Complete the 'plusMinus' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def plusMinus(arr):
    # calculate ratios of pos, neg, zero
    # run through each integer
    length = len(arr)
    positive = 0
    negative = 0
    zero = 0
    for a in arr:
        if a > 0:
            positive += 1
        elif a < 0:
            negative += 1
        else:
            zero += 1
    # print dec value of ratio on new line with 6 places after dec
    print(f"{(positive / length):.6f}")
    print(f"{(negative / length):.6f}")
    print(f"{(zero / length):.6f}")


if __name__ == '__main__':
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr)
