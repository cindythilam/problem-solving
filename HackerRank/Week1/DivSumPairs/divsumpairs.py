#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'divisibleSumPairs' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER k
#  3. INTEGER_ARRAY ar
#

def divisibleSumPairs(n, k, ar):
    # initialize pairs to 0
    pairs = 0
    # nested loop where i is the first index
    for x in range(n):
        # nested loop where j is the second index (i+1 because i<j)
        for y in range(x+1, n):
            # if the sum of the pair is divisible by k
            if (ar[x] + ar[y]) % k == 0:
                # if yes, increment the pairs count
                pairs += 1
    # return an int: the # of pairs
    return pairs

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    k = int(first_multiple_input[1])

    ar = list(map(int, input().rstrip().split()))

    result = divisibleSumPairs(n, k, ar)

    fptr.write(str(result) + '\n')

    fptr.close()
