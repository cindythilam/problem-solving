#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'sockMerchant' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER_ARRAY ar
#


def sockMerchant(n, ar):
    # Pile of socks to be paired by color
    # Each color = #
    # How many pairs of socks with matching colors?

    # Initialize a dictionary to store count of each color
    sock_count = {}
    pairs = 0

    # Iterate over each sock in the array
    for color in ar:
        if color in sock_count:
            sock_count[color] += 1
        else:
            sock_count[color] = 1

    # Check for pairs
        if sock_count[color] % 2 == 0:
            pairs += 1

    # Return # of pairs
    return pairs

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    ar = list(map(int, input().rstrip().split()))

    result = sockMerchant(n, ar)

    fptr.write(str(result) + '\n')

    fptr.close()
