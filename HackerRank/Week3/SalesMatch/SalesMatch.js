'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'sockMerchant' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER_ARRAY ar
 */

function sockMerchant(n, ar) {
    // Create object to store count of each color
    let sockCount = {};
    let pairs = 0;

    // Iterate over each sock in the array
    for (let i = 0; i < n; i++) {
        let color = ar[i];

        // If color is in sockCount, increment count
        if (color in sockCount) {
            sockCount[color] += 1;
        } else {
            // Else, initialize count for this color to 1
            sockCount[color] = 1;
        }

        // Check for pairs
        if (sockCount[color] % 2 === 0) {
            pairs += 1;
        }
        }

        // Return # of pairs
        return pairs;
}


function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const n = parseInt(readLine().trim(), 10);

    const ar = readLine().replace(/\s+$/g, '').split(' ').map(arTemp => parseInt(arTemp, 10));

    const result = sockMerchant(n, ar);

    ws.write(result + '\n');

    ws.end();
}
